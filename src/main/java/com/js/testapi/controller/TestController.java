package com.js.testapi.controller;

import com.js.testapi.model.TestRequest;
import com.js.testapi.model.TestUsernameDupCheck;
import com.js.testapi.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("v1/test/")
public class TestController {
    private final TestService testService;

    @GetMapping("/id/duplicate/check")
    public TestUsernameDupCheck getDupCheck(@RequestParam String username){
           return   testService.getDupCheck(username);
    }

    @PostMapping("/new")
    public String setMemberJoin(@RequestBody TestRequest request)throws Exception{
        testService.setMemberJoin(request);

        return "가입이 완료되었습니다.";}
}
