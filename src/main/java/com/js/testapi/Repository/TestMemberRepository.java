package com.js.testapi.Repository;

import com.js.testapi.entity.TestMember;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestMemberRepository extends JpaRepository<TestMember , Long> {
    long countByUsername(String username);

}
