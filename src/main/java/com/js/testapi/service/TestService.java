package com.js.testapi.service;

import com.js.testapi.Repository.TestMemberRepository;
import com.js.testapi.entity.TestMember;
import com.js.testapi.model.TestRequest;
import com.js.testapi.model.TestUsernameDupCheck;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class TestService {
    private final TestMemberRepository testMemberRepository;

    /** bool 값을 json 값으로 리팩터링하는 과정
     * isNewUsername을 model에 받을 그릇을 만들어 담아 보내는 방
     */

    public TestUsernameDupCheck getDupCheck(String username){
        TestUsernameDupCheck result = new TestUsernameDupCheck();
        result.setIsNew(isNewUsername(username));

        return result;
    }

    /**
     *
     * @param request 회원가입창에서 고객이 입력한 정보
     * @throws Exception
     * 아이디가 중복된 경
     * 비밀번호 확인과 비밀 번호 입력이 일치하지 않을 경우
     */

    public void setMemberJoin(TestRequest request) throws Exception{
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();
        TestMember addData = new TestMember();
        addData.setUsername(request.getUsername());
        addData.setPassword(request.getPassword());
        addData.setName(request.getName());
        addData.setDateJoin(LocalDateTime.now());

        testMemberRepository.save(addData);
    }

    /**
     *
     * @param username 아이디
     * @return true 신규 아이디 / false 중복 아이디
     */

    private boolean isNewUsername(String username){
        long dupCount= testMemberRepository.countByUsername(username);
        return dupCount <=0;
    }

}