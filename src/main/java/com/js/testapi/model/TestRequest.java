package com.js.testapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TestRequest {
    private String username;

    private String password;

    private String passwordRe;

    private String name;
}
