package com.js.testapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TestUsernameDupCheck {

    private Boolean isNew;
}
